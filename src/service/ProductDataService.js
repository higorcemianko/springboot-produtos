import axios from "axios";

const PRODUCT_API_URL = "http://localhost:8080/springboot-0.0.1-SNAPSHOT";
const PRODUCT = "produtos";


class ProductDataService {
  returnAllProducts() {
    return axios.get(`${PRODUCT_API_URL}/${PRODUCT}`);
  }

  deleteProduct(id) {
    return axios.delete(`${PRODUCT_API_URL}/${PRODUCT}/${id}`);
  }

  retrieveProduct(id) {
    return axios.get(`${PRODUCT_API_URL}/${PRODUCT}/${id}`);
  }

  updateProduct(id, product) {
    return axios.put(`${PRODUCT_API_URL}/${PRODUCT}/${id}`, product);
  } 

createProduct(product) {
    return axios.post(`${PRODUCT_API_URL}/${PRODUCT}`, product);
  }
}

export default new ProductDataService();