import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history", // Use browser history
  routes: [
    {
      path: "/",
      name: "Home",
      component: () => import("./components/ProductList")
    },
    {
      path: "/produtos",
      name: "Lista de Produtos",
      component: () => import("./components/ProductList")
    },
    {
      path: "/produtos/:id",
      name: "Detalhes Produtos",
      component: () => import("./components/ProductComponent")
    }
  ]
});

export default router;